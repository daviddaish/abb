FROM ros:kinetic-ros-core-xenial
SHELL ["/bin/bash", "-c"]

# Setup catkin workspace
RUN mkdir -p /home/catkin_ws/src
WORKDIR /home/catkin_ws
RUN source /opt/ros/kinetic/setup.bash && catkin_make
RUN echo "source /home/catkin_ws/devel/setup.bash" >> ~/.bashrc

RUN apt-get update
RUN apt-get install -y liburdfdom-tools ros-kinetic-xacro

ADD . src/  
