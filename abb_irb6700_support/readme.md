todo:

* Add the `launch` and `test` directories back in.
* Test all `launch` files.
* Reduce the number of polygons within the collision meshes to speed up
  computation time.
